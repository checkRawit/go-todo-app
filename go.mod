module gitlab.com/checkRawit/go-todo-app

go 1.15

require (
	github.com/Kamva/mgm/v3 v3.0.1
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/manifoldco/promptui v0.7.0
	go.mongodb.org/mongo-driver v1.3.4
)
