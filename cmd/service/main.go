package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/checkRawit/go-todo-app/app"
)

func main() {
	taskService, err := app.NewTaskService(config.DatabaseURI, config.DatabaseName)
	if err != nil {
		panic(err)
	}

	tasks, err := taskService.GetAllTasks()
	if err != nil {
		panic(err)
	}

	b, err := json.MarshalIndent(tasks, "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", b)
}
