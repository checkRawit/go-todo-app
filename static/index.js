function setIsDone(id, isDone) {
  fetch(`/api/tasks/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ isDone }),
  }).then((res) => location.reload());
}

function createTask(event) {
  event.preventDefault();
  fetch("/api/tasks", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ message: event.target.elements["message"].value }),
  }).then((res) => location.reload());
}

function deleteTask(id) {
  fetch(`/api/tasks/${id}`, {
    method: "DELETE",
  }).then((res) => location.reload());
}
