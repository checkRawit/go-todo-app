package app

import (
	"github.com/Kamva/mgm/v3"
)

// Task is a task model by github.com/Kamva/mgm/v3
type Task struct {
	mgm.DefaultModel `bson:",inline"`
	Message          string `json:"message" bson:"message"`
	IsDone           bool   `json:"isDone" bson:"isDone"`
}

func (Task) CollectionName() string {
	return "tasks"
}
