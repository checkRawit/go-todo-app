package app

import (
	"github.com/Kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type TaskService struct {
	taskCollection *mgm.Collection
}

func NewTaskService(uri string, dbName string) (TaskService, error) {
	// Create mongodb client
	err := mgm.SetDefaultConfig(nil, "dbName")
	client, err := mgm.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return TaskService{}, err
	}

	taskCollection := mgm.NewCollection(client.Database(dbName), Task{}.CollectionName())

	return TaskService{taskCollection: taskCollection}, nil
}

// CreateTask is a function that create task by a given message
func (s TaskService) CreateTask(message string) (Task, error) {
	task := Task{Message: message, IsDone: false}

	if err := s.taskCollection.Create(&task); err != nil {
		return Task{}, err
	}

	return task, nil
}

// GetAllTasks is a function that return all tasks
func (s TaskService) GetAllTasks() ([]Task, error) {
	var tasks []Task
	if err := s.taskCollection.SimpleFind(&tasks, bson.M{}); err != nil {
		return nil, err
	}

	return tasks, nil
}

func (s TaskService) GetTask(id string) (Task, error) {
	// Get the specific task
	task := Task{}
	if err := s.taskCollection.FindByID(id, &task); err != nil {
		return Task{}, err
	}

	return task, nil
}

func (s TaskService) DeleteTask(id string) error {
	// Get the specific task
	task := Task{}
	if err := s.taskCollection.FindByID(id, &task); err != nil {
		return err
	}

	// Delete
	if err := s.taskCollection.Delete(&task); err != nil {
		return err
	}

	return nil
}

func (s TaskService) UpdateTaskIsDone(id string, isDone bool) error {
	// Get the specific task
	task := Task{}
	if err := s.taskCollection.FindByID(id, &task); err != nil {
		return err
	}

	// Update Task.IsDone
	task.IsDone = isDone
	if err := s.taskCollection.Update(&task); err != nil {
		return err
	}

	return nil
}
