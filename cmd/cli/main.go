package main

import (
	"encoding/json"
	"fmt"

	"github.com/manifoldco/promptui"
	"gitlab.com/checkRawit/go-todo-app/app"
)

func main() {

	taskService, err := app.NewTaskService(config.DatabaseURI, config.DatabaseName)
	if err != nil {
		panic(err)
	}

	prompt := promptui.Select{
		Label: "Select a command",
		Items: []string{"Read All", "Create", "Toggle isDone", "Delete", "Exit"},
	}

Loop:
	for {
		_, result, err := prompt.Run()
		if err != nil {
			panic(err)
		}

		switch result {
		case "Read All":
			tasks, err := taskService.GetAllTasks()
			if err != nil {
				panic(err)
			}

			b, err := json.MarshalIndent(tasks, "", "  ")
			if err != nil {
				panic(err)
			}

			fmt.Printf("%s\n", b)
		case "Create":
			// Prompt message
			messagePrompt := promptui.Prompt{Label: "Type a message"}
			message, err := messagePrompt.Run()
			if err != nil {
				panic(err)
			}

			// Create task
			task, err := taskService.CreateTask(message)
			if err != nil {
				panic(err)
			}

			b, err := json.MarshalIndent(task, "", "  ")
			if err != nil {
				panic(err)
			}

			fmt.Printf("%s\n", b)
		case "Delete":
			// Prompt ID
			idPrompt := promptui.Prompt{Label: "Type a ID"}
			id, err := idPrompt.Run()
			if err != nil {
				panic(err)
			}

			// Delete task
			if err = taskService.DeleteTask(id); err != nil {
				panic(err)
			}
		case "Toggle isDone":
			// Prompt ID
			idPrompt := promptui.Prompt{Label: "Type a ID"}
			id, err := idPrompt.Run()
			if err != nil {
				panic(err)
			}

			// Get that task to see isDone value
			task, err := taskService.GetTask(id)
			if err != nil {
				panic(err)
			}

			// Update isDone to opposite value
			if err = taskService.UpdateTaskIsDone(task.ID.Hex(), !task.IsDone); err != nil {
				panic(err)
			}
		case "Exit":
			break Loop
		}
	}
}
