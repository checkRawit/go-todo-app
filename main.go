package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/checkRawit/go-todo-app/app"
)

func main() {

	taskService, err := app.NewTaskService(config.DatabaseURI, config.DatabaseName)
	if err != nil {
		panic(err)
	}

	t, err := template.ParseGlob("./views/*.gohtml")
	if err != nil {
		panic(err)
	}

	// Create router
	router := httprouter.New()

	// Serve static folder
	router.ServeFiles("/static/*filepath", http.Dir("./static"))

	// Create task endpoint
	router.POST("/api/tasks", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		// Parse body
		var body struct {
			Message string `json:"message"`
		}
		if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
			http.Error(w, "Cannot decode body", 400)
			return
		}

		task, err := taskService.CreateTask(body.Message)
		if err != nil {
			http.Error(w, "Cannot create task", 500)
			return
		}

		if err = json.NewEncoder(w).Encode(task); err != nil {
			http.Error(w, "Cannot write task to response body", 500)
			return
		}
	})

	// Delete task
	router.DELETE("/api/tasks/:id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if err := taskService.DeleteTask(ps.ByName("id")); err != nil {
			http.Error(w, "Cannot delete task", 500)
			return
		}

		// Send empty body
		w.Write([]byte{})
	})

	// Update task
	router.PATCH("/api/tasks/:id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var body struct {
			Message *string `json:"message"`
			IsDone  *bool   `json:"isDone"`
		}

		if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
			http.Error(w, "Cannot decode body", 400)
			return
		}

		if body.Message != nil {
			http.Error(w, "Patch task messege is not implement", 501)
			return
		}
		if body.IsDone != nil {
			// Get the exist task
			task, err := taskService.GetTask(ps.ByName("id"))
			if err != nil {
				http.Error(w, "Cannot get task", 500)
				return
			}

			// Toggle isDone value
			if err = taskService.UpdateTaskIsDone(task.ID.Hex(), !task.IsDone); err != nil {
				http.Error(w, "Cannot update task isDone value", 500)
				return
			}
		}

		// Send empty body
		w.Write([]byte{})
	})

	// Homepage
	router.GET("/", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

		// start := time.Now()

		// Get Tasks
		tasks, err := taskService.GetAllTasks()
		if err != nil {
			http.Error(w, "Cannot Get all tasks", 500)
			return
		}
		// log.Println("After GetAllTasks(): ", time.Since(start))

		// Write a template
		t.ExecuteTemplate(w, "main.gohtml", struct{ Tasks []app.Task }{Tasks: tasks})
		// log.Println("After ExecuteTemplate(): ", time.Since(start))

	})

	// Listening
	log.Println("Listening on port", config.ServerPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.ServerPort), router))
}
