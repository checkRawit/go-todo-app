FROM golang:1.15 AS builder

WORKDIR /app

# Download dependencies
COPY go.* ./
RUN go mod download

# Compile source
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -v -o server

# Image for run server
FROM alpine:3
RUN apk add --no-cache ca-certificates

WORKDIR /app

COPY --from=builder /app/server ./
COPY views ./views/
COPY static ./static

# RUN ls /app -al && ls /app/views -al && ls /app/static -al

CMD [ "./server" ]